<?php

use Illuminate\Database\Seeder;
use App\Models\Place;

class PlacesTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('places')->insert([
            'name' => 'Ideil',
            'slug' => 'ideil',
            'description' => 'the web developers',
            'rating' => 10,
            'likes' => 100,
            'dislikes' => 0,
            'lat' => '50.753952',
            'lng' => '25.349860',
        ]);
        factory(Place::class, 300)->create();
    }
}
