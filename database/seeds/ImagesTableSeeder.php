<?php

use Illuminate\Database\Seeder;
use App\Models\Image;

class ImagesTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('images')->insert([
            'path' => 'https://ideil.com/src/img/opengraph.jpg',
            'images_type' => 'App\Models\Place',
            'images_id' => 1,
        ]);
        DB::table('images')->insert([
            'path' => 'https://laravel.com/assets/img/partner-img-ideil.png',
            'images_type' => 'App\Models\Place',
            'images_id' => 1,
        ]);
        factory(Image::class, 1000)->create();
    }
}
