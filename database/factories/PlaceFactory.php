<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Models\Place;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Place::class, function (Faker $faker) {
    return [
        'name' => $faker->unique()->city . ', ' . $faker->state,
        'slug' => $faker->unique()->userName,
        'description' => $faker->text(200),
        'rating' => rand(1,10),
        'likes' => rand(0,100),
        'dislikes' => rand(0,10),
        'lat' => $faker->latitude(29, 60),
        'lng' => $faker->longitude(17 ,53),
    ];
});
