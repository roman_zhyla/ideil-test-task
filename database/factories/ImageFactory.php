<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Models\Image;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Image::class, function (Faker $faker) {
    return [
        'path' => $faker->imageUrl(300, 300),
        'images_type' => 'App\Models\Place',
        'images_id' => rand(2,301),
    ];
});
