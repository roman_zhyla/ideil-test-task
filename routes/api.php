<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::namespace('Api')->group(function () {
    Route::get('/users/token', 'UsersController@token');

    Route::get('/places/comments', 'PlacesController@comments');
    Route::get('/places/content', 'PlacesController@content');
    Route::post('/places/images', 'PlacesController@images')->name('places.images');
    Route::post('/places/likes', 'PlacesController@likes')->name('places.likes');
    Route::resource('/places', 'PlacesController');

    Route::post('/comments/store', 'CommentsController@store')->name('comments.store');
    Route::post('/comments/edit', 'CommentsController@edit')->name('comments.edit');

    Route::get('images/rebuild/{type}', 'ImagesController@rebuildImages')->name('images.rebuild');
    Route::post('images/upload', 'ImagesController@upload')->name('images.upload');
    Route::resource('images', 'ImagesController');
});


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/{any}', 'SpaController@index')
    ->where('any', '.*');
