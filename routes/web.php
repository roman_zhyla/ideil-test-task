<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
// Admin routes
Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin'], function() {
    Route::group(['middleware' => 'auth', 'is_admin'], function() {
        Route::get('/', 'PlacesController@index')->name('admin.dashboard');

        Route::resource('places', 'PlacesController');
    });
});

Route::get('/{any}', 'SpaController@index')->where('any', '.*');
