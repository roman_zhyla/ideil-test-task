window.Vue = require('vue');
import VueRouter from 'vue-router'

Vue.use(VueRouter)

import App from './views/App'
import Home from './views/Home'
import Places from './views/Places'
import Place from './views/Place'
import NotFound from './views/NotFound';

Vue.component('pagination', require('laravel-vue-pagination'));
Vue.component('images-component', require('./components/ImagesComponent.vue').default);
Vue.component('comments-component', require('./components/CommentsComponent.vue').default);

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/places',
            name: 'places',
            component: Places
        },
        {
            path: '/places/:slug',
            name: 'place',
            component: Place
        },
        {
            name:'map',
            path:'/:slug',
            component: Home,
            props: true
        },
        {
            path: '/404',
            name: '404',
            component: NotFound
        }
    ],
});

const app = new Vue({
    el: '#app',
    components: { App },
    router,
});
