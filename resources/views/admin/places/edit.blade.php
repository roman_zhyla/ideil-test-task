@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <h1>Edit place: {{$place->name}}</h1>
        </div>
        {{ Form::open(['method' => 'PUT', 'files' => true, 'route' => ['admin.places.update', $place]]) }}

        <div class="body-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-success bg-white shadow  p-2">
                        <div class="panel-body padding-15">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        {{ Form::label('name', 'Name', ['class' => 'control-label']) }}
                                        @if ($errors->has('name'))
                                            <span style="color:red;">{{ $errors->first('name') }}</span>
                                        @endif
                                        {{ Form::text('name', $place->name, ['class' => 'form-control']) }}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        {{ Form::label('slug', 'Slug', ['class' => 'control-label']) }}
                                        @if ($errors->has('slug'))
                                            <span style="color:red;">{{ $errors->first('slug') }}</span>
                                        @endif
                                        {{ Form::text('slug', $place->slug, ['class' => 'form-control']) }}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        {{ Form::label('lat', 'Latitude', ['class' => 'control-label']) }}
                                        @if ($errors->has('lat'))
                                            <span style="color:red;">{{ $errors->first('lat') }}</span>
                                        @endif
                                        {{ Form::text('lat', $place->lat, ['class' => 'form-control']) }}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        {{ Form::label('lng', 'Longitude', ['class' => 'control-label']) }}
                                        @if ($errors->has('lng'))
                                            <span style="color:red;">{{ $errors->first('lng') }}</span>
                                        @endif
                                        {{ Form::text('lng', $place->lng, ['class' => 'form-control']) }}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        {{ Form::label('rating', 'Rating', ['class' => 'control-label']) }}
                                        @if ($errors->has('rating'))
                                            <span style="color:red;">{{ $errors->first('rating') }}</span>
                                        @endif
                                        {{ Form::select('rating', $ratings, $place->rating, ['class' => 'form-control']) }}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        {{ Form::label('description', 'Description', ['class' => 'control-label']) }}
                                        @if ($errors->has('description'))
                                            <span style="color:red;">{{ $errors->first('description') }}</span>
                                        @endif
                                        {{ Form::textarea('description', $place->description, ['class' => 'form-control']) }}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="slug" class="control-label">Images</label>
                                        <images-component
                                            item-id="{{$place->id}}"
                                            controller="places"
                                            images-folder="places"
                                        ></images-component>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button class="btn-success" title="Save" type="submit">Save</button>
                    </div>
                </div>
            </div>
        </div>
        {{ Form::close() }}
    </div>
@endsection
