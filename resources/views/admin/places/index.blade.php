@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <h1>Dashboard</h1>
        </div>
        <div class="container bg-white shadow">
            <div class="row justify-content-end">
                <a href="{{ route("admin.places.create") }}" class="btn btn-success">+ Add</a>
            </div>
            <div class="row justify-content-center font-weight-bold p-2">
                <div class="col-2">
                    id
                </div>
                <div class="col-4">
                    name
                </div>
                <div class="col-2">
                    coordinates
                </div>
                <div class="col-4"></div>
            </div>
            @foreach($places as $place)
                <div class="row p-2">
                    <div class="col-2">
                        {{$place->id}}
                    </div>
                    <div class="col-4">
                        {{$place->name}}
                    </div>
                    <div class="col-2">
                        {{$place->lat}}<br>{{$place->lng}}
                    </div>
                    <div class="col-4">
                        <a href="{{ route("admin.places.show", $place->id) }}" class="btn btn-primary">Show</a>
                        <a href="{{ route("admin.places.edit", $place->id) }}" class="btn btn-warning">Edit</a>
                        {{ Form::open(['method' => 'DELETE', 'route' => ['admin.places.destroy', $place], 'style' => 'display:inline']) }}
                        {{ Form::submit('Delete', ['class' => 'btn btn-danger', 'style' => 'display:inline-block']) }}
                        {{ Form::close() }}
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
