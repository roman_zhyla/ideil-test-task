@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <h1>{{$place->name}}</h1>
        </div>
        <div class="body-content">
            <div class="row justify-content-end">
                <a href="{{ route("admin.places.edit", $place->id) }}" class="btn btn-warning">Edit</a>
                <a href="/{{$place->slug}}" target="_blank" class="btn btn-success">Show on site</a>
            </div>
            <div class="row">
                <div class="col-4">
                    <div class="panel panel-success bg-white shadow  p-2">
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <p>Name: {{ $place->name}}</p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <p>Slug: {{ $place->slug}}</p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <p>Latitude: {{ $place->lat}}</p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <p>Longitude: {{ $place->lng}}</p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <p>Rating: {{ $place->rating}}</p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <p>Description: {{ $place->description}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-8">
                    <div class="panel panel-success bg-white shadow  p-2">
                        <div class="row">
                            <div class="col-md-12">
                                <h3>Images</h3>
                                @if($place->images)
                                    @foreach($place->images as $image )
                                        <div class="form-group d-inline-block">
                                            <img class="d-block" style="width: 50%;" src="{{$image->path}}" alt="">
                                        </div>
                                    @endforeach
                                @else
                                    No images yet...
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-top: 1.5rem;">
                <div class="col-12">
                    <div class="panel panel-success bg-white shadow  p-2">
                        <div class="row">
                            <div class="col-md-12">
                                <h3>Comments</h3>
                                <comments-component item-id="{{$place->id}}"></comments-component>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{ Form::close() }}
    </div>
@endsection
