<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class PlaceResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'slug' => $this->slug,
            'description' => $this->description,
            'rating' => $this->rating,
            'images' => $this->images,
            'comments' => $this->comments,
            'likes' => $this->likes,
            'dislikes' => $this->dislikes,
            'position' => [
                'lat' => $this->lat,
                'lng' => $this->lng,
            ],
            'infoOptions' => [
                'pixelOffset' => [
                    'width' => 0,
                    'height' => 0
                ],
            ]
        ];
    }
}
