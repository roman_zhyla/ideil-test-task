<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Comment;
use Illuminate\Http\Request;
use App\Http\Requests\CommentStoreRequest;

class CommentsController extends Controller
{

    /**
     * @param CommentStoreRequest $request
     */
    public function store(CommentStoreRequest $request)
    {
       Comment::create($request->all());
    }
}
