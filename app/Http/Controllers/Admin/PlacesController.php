<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Place;
use Illuminate\Http\Request;
use App\Http\Requests\PlaceStoreRequest;
use App\Http\Requests\PlaceUpdateRequest;
use App\Models\Image;

class PlacesController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $places = Place::orderByDesc('updated_at')->get();
        return view('admin.places.index', compact('places'));
    }

    /**
     * @param Place $place
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Place $place){
        return view('admin.places.show', compact('place'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $ratings = [
            1 => '★☆☆☆☆☆☆☆☆☆',
            2 => '★★☆☆☆☆☆☆☆☆',
            3 => '★★★☆☆☆☆☆☆☆',
            4 => '★★★★☆☆☆☆☆☆',
            5 => '★★★★★☆☆☆☆☆',
            6 => '★★★★★★☆☆☆☆',
            7 => '★★★★★★★☆☆☆',
            8 => '★★★★★★★★☆☆',
            9 => '★★★★★★★★★☆',
            10 => '★★★★★★★★★★',
        ];
        return view('admin.places.create', compact('ratings'));
    }

    /**
     * @param PlaceStoreRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(PlaceStoreRequest $request)
    {
        $place = Place::create($request->except('images'));
        $this->saveImage($request, $place);
        return redirect()->route('admin.places.index');
    }

    /**
     * @param Place $place
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Place $place)
    {
        $ratings = [
            1 => '★☆☆☆☆☆☆☆☆☆',
            2 => '★★☆☆☆☆☆☆☆☆',
            3 => '★★★☆☆☆☆☆☆☆',
            4 => '★★★★☆☆☆☆☆☆',
            5 => '★★★★★☆☆☆☆☆',
            6 => '★★★★★★☆☆☆☆',
            7 => '★★★★★★★☆☆☆',
            8 => '★★★★★★★★☆☆',
            9 => '★★★★★★★★★☆',
            10 => '★★★★★★★★★★',
        ];
        return view('admin.places.edit', compact('place', 'ratings'));
    }

    /**
     * @param PlaceStoreRequest $request
     * @param Place $place
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(PlaceUpdateRequest $request, Place $place)
    {
        Place::updateOrCreate(['id' => $place->id],$request->except('images'));
        $this->saveImage($request, $place);
        return redirect()->route('admin.places.index');
    }

    /**
     * @param Place $place
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Place $place)
    {
        $place->delete();
        return redirect()->route('admin.places.index');
    }

    /**
     * @param Request $request
     * @param Place $place
     */
    private function saveImage(Request $request, Place $place){
        $place->images()->delete();
        if(isset($request->images)){
            foreach ($request->images as $image) {
                $createArray = [
                    'path' => $image,
                    'images_id' => $place->id,
                    'images_type' => Place::class,
                ];
                Image::create($createArray);
            }
        }
    }
}
