<?php
namespace App\Http\Controllers\Api;

use App\Models\Place;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\PlaceResource;
use App\Models\Image;
use App\Http\Resources\ImageResource;
use App\Http\Resources\CommentResource;

class PlacesController extends Controller
{
    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        if(isset($request->paginate)){
            return PlaceResource::collection(Place::paginate(30));
        }
        return PlaceResource::collection(Place::get());
    }

    /**
     * @param Place $place
     * @return PlaceResource
     */
    public function show(string $slug)
    {
        $ratings = [
            1 => '★☆☆☆☆☆☆☆☆☆',
            2 => '★★☆☆☆☆☆☆☆☆',
            3 => '★★★☆☆☆☆☆☆☆',
            4 => '★★★★☆☆☆☆☆☆',
            5 => '★★★★★☆☆☆☆☆',
            6 => '★★★★★★☆☆☆☆',
            7 => '★★★★★★★☆☆☆',
            8 => '★★★★★★★★☆☆',
            9 => '★★★★★★★★★☆',
            10 => '★★★★★★★★★★',
        ];

        $place = Place::where('slug', $slug)->with('images', 'comments')->first();
        $place->rating = $ratings[$place->rating];
        $place->comments = $place->comments->where('active', true);
        return new PlaceResource($place);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function images(Request $request){
        if(!$request->id)   return response($request->toArray(),400);
        $images = Image::where('images_type', Place::class)->where('images_id', $request->id)->get();
        return ImageResource::collection($images);
    }

    /**
     * @param Request $request
     */
    public function likes(Request $request){
        if(!empty($request->type) && !empty($request->action)){
            $type = $request->type;
            $elseType = ($type == 'likes' ? 'dislikes' : 'likes');
            $place = Place::whereId($request->place_id)->first();
            if($request->action == 'add'){
                $place->$type += 1;
                if($place->$elseType > 0){
                    $place->$elseType -= 1;
                }
            }else{
                if($place->$type > 0){
                    $place->$type -= 1;
                }
            }
            $place->save();
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function comments(Request $request){
        if(isset($request->id)) {
            $place = Place::where('id', $request->id)->with('comments')->first();
            return CommentResource::collection($place->comments()->get());
        }
    }

    /**
     * @param Request $request
     * @return string
     */
    public function content(Request $request){
        if(isset($request->id)){
            $place = Place::where('id', $request->id)->with('images')->first();
            $rating = '';
            $images = 'No images yet....';
            $comments = 'No comments yet....';
            for ($i = 1; $i<= 10; $i++){
                if($i <= $place->rating){
                    $rating .= '★';
                }else{
                    $rating .= '☆';
                }
            }
            if(!empty($place->images) && count($place->images) > 0){
                $images = '';
                foreach ($place->images as $image) {
                    $images .= "<img style=\"display: inline-block; width:50%;\" src=\"$image->path\" />";
                }
            }

            if(!empty($place->comments) && count($place->comments->where('active', true)) > 0){
                $comments = '';
                foreach ($place->comments->where('active', true) as $comment) {
                    $comments .= "<div style='padding: 0 5px; border-bottom: 1px solid #000;'>
                                    <p>$comment->comment</p>
                                  </div>";
                }
            }

            $html = "<div id=\"content\" style='max-width: 440px; max-height: 250px;'>
                        <h1>$place->name</h1>
                        <div id=\"bodyContent\">
                            <p class=\"info\">
                                <p><b>Description:</b>$place->description</p>
                                <p><b>Rating:</b> $rating</p>
                                <p><b>Likes:</b> $place->likes | <a class='d-inline text-success' href='/places/$place->slug'>👍</a></p>
                                <p><b>Dislikes:</b> $place->dislikes | <a class='d-inline text-success' href='/places/$place->slug'>👎</a></p>
                                <p><b>Gallery:</b></p>
                                <div>$images</div>
                                <p><b>Comments:</b></p>
                                <div>$comments</div>
                                <div><a class='d-inline text-success' href='/places/$place->slug'>Add new comment</a></div>
                            </div>
                        </div>
                    </div>";
            return $html;
        }
    }
}
