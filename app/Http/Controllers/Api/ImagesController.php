<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Services\ImageService;


class ImagesController extends Controller
{
    /**
     * @param Request $request
     * @param ImageService $imageService
     * @return string
     */
    public function upload(Request $request, ImageService $imageService){
        return $imageService->storeImage($request->image, $request->folder);
    }
}
