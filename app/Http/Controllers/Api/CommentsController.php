<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Comment;
use Illuminate\Http\Request;

class CommentsController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(isset($request->user)) {
            $comment = Comment::where('user_token', $request->user)->where('place_id', $request->place_id)->first();
            if(empty($comment)){
                return Comment::create([
                    'user_token' => $request->user,
                    'place_id' => $request->place_id,
                    'comment' => $request->text,
                    'active' => 1,
                ]);
            }
            return response('Comment already exist',409);
        }
        return response('Bad Request',409);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        if(isset($request->id) && !empty($request->type)) {
            return Comment::updateOrCreate(['id' => $request->id], [$request->type => $request->value]);
        }
        return response('Bad Request',409);
    }
}
