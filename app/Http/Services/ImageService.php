<?php

namespace App\Http\Services;

class ImageService
{
    public function storeImage($image, $folder){
        $file = $image->store('public/images/' . $folder);
        $path = '/'. str_replace('public/', 'storage/', $file);
        return $path;
    }
}
